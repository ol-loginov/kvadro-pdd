$('ul.topic-list .topic-options')
    .on('click', '[data-event-handler=deleteTopic]', function () {
        var el = $(this), topic = el.data('event-param');
        inlineEdit
            .post({name: 'delete-topic', topic: topic})
            .done(function () {
                el.closest('li').remove();
            });
    });
