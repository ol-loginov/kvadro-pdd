$('ul.ticket-list .ticket-options')
    .on('click', '[data-event-handler=deleteTicket]', function () {
        var el = $(this), ticket = el.data('event-param');
        inlineEdit.post({name: 'delete-ticket', ticket: ticket})
            .done(function () {
                el.closest('li').remove();
            });
    });
