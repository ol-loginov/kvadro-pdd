//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/array/shuffle [v1.0]
function shuffle(o) { //v1.0
    for (var j, x, i = o.length; i;) {
        j = Math.floor(Math.random() * i);
        x = o[--i];
        o[i] = o[j];
        o[j] = x;
    }
    return o;
}

TrainingSettings = (function () {
    var TEMPLATE =
        '<div id="settingsDialog" class="modal">' +
        ' <div class="modal-dialog">' +
        '  <div class="modal-content">' +
        '   <div class="modal-header">' +
        '    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '    <h4 class="modal-title">Настройки текущей сессии</h4>' +
        '   </div>' +
        '   <div class="modal-body">INNER</div>' +
        '   <div class="modal-footer">' +
        '    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>' +
        '   </div>' +
        '  </div>' +
        ' </div>' +
        '</div>';

    function getTopicProgress(state) {
        return '<span class="label label-success" style="margin-right:1em;">100%</span><span style="margin-right:1em;" class="badge">' + state.questions + '</span>';
    }

    function getTopicLabel(k) {
        return '<span>' + (k == '' ? 'Вопросы без категории' : k) + '</span>';
    }

    return {
        state: {},
        load: function (theme) {
            var stateEmpty = {
                topics: {}
            };
            this.state = $.extend(stateEmpty, localStorage.getItem('training-settings-' + theme));
        },
        save: function (theme) {
            localStorage.setItem('training-settings-' + theme, this.state);
        },
        setTopicList: function (topics) {
            var topicStateList = this.state.topics;
            $.each(topics, function (_, k) {
                topicStateList[k.topic] = $.extend({show: true, progress: '', questions: 0}, topicStateList[k.topic], k);
            });
        },
        configure: function () {
            var self = this;

            function renderTopicCheckbox(k) {
                var checked = (self.state.topics[k].show) ? ' checked="checked" ' : '';
                var label = getTopicLabel(k);
                var progress = getTopicProgress(self.state.topics[k]);
                return '<div class="checkbox"><label><input type="checkbox" name="topicShow" value="' + k + '" ' + checked + '>' + progress + label + '</label></div>';
            }

            var html = '';

            html += '<fieldset><legend style="margin-bottom: 0;border-bottom: none;">Категории для тренировки</legend>';
            $.each(self.state.topics, function (k) {
                html += renderTopicCheckbox(k);
            });
            html += '</fieldset>';

            $(TEMPLATE.replace('INNER', html))
                .on('shown.bs.modal', function (e) {
                    $(e.target).find('input[name=topicShow]').change(function () {
                        var topic = $(this).val();
                        self.state.topics[topic].show = $(this).prop('checked');
                        return false;
                    });
                })
                .on('hidden.bs.modal', function (e) {
                    $(e.target).remove();
                })
                .modal();
        }
    };
})();

Training = (function () {
    var respondQuestionEvents = true;
    var progressCorrect = 0;
    var progressTotal = 0;

    var questionCache = {};

    function loadQuestionAjax(theme, index) {
        if (index in questionCache) {
            return $.when(questionCache[index]);
        }
        return $
            .ajax({method: 'POST', data: {name: 'get-question', theme: theme, question: index}})
            .done(function (resp) {
                questionCache[index] = resp;
            });
    }

    function updateProgress(el) {
        el
            .empty()
            .append('<span class="correct">' + progressCorrect + '</span> - <span class="wrong">' + (progressTotal - progressCorrect) + '</span>');
    }

    function renderQuestion(container, resp) {
        var html = '';

        html += '<ul class="topic-list list-unstyled">';
        $.each(resp.topics, function () {
            html += '<li><span class="label label-info">' + this + '</span></li>';
        });
        html += '</ul>';

        html += '<h1>' + resp.question.text + '</h1>';
        if (resp.question.picture) {
            html += '<img class="question-image" src="/picture/' + resp.question.picture + '">';
        }
        shuffle(resp.answers);
        html += '<div class="answer-form"><div class="btn-group-vertical" role="group">';
        $.each(resp.answers, function (index) {
            html += '<button class="btn btn-default" type="submit" name="answer" value="' + index + '">' + this.text + '</button>';
        });
        html += '</div></div>';

        container
            .empty()
            .append(html);
    }

    function saveState(theme, state) {

    }

    function loadState(theme) {
        return {
            results: []
        }
    }

    var queue = {
        array: [],
        push: function (number, correct) {
            var priority = correct ? this.array.length + 1 : Math.ceil(this.array.length / 8) + 1;
            this.array.push([number, priority]);
        },
        next: function () {
            this.array.sort(function (a, b) {
                return a[1] - b[1]
            });
            var item = this.array.shift();
            for (var i in this.array) {
                this.array[i][1] -= 1;
            }
            return item[0];
        }
    };

    return {
        state: null,
        showNextQuestion: function (delay) {
            this._showQuestion(queue.next(), delay);
        },
        toggleIndicatorClass: function (index, className, show) {
            var selector = index ? 'li[data-index=' + index + ']' : 'li[data-index]';
            this.indicator.find(selector).toggleClass(className, show);
        },
        _showQuestion: function (index, delay) {
            var self = this;
            if (typeof delay != "number") delay = 100;
            respondQuestionEvents = false;
            setTimeout(function () {
                self.currentQuestionIndex = index;
                self.toggleIndicatorClass(0, "active", false);
                loadQuestionAjax(self.theme, index)
                    .done(function (resp) {
                        self.currentQuestion = resp;
                        self.currentQuestionAnswered = false;
                        self.toggleIndicatorClass(self.currentQuestionIndex, "active", true);
                        renderQuestion(self.container, resp);
                    })
                    .always(function () {
                        respondQuestionEvents = true;
                    });
            }, delay);
        },
        start: function (options) {
            this.theme = options.theme;
            TrainingSettings.load(this.theme);
            TrainingSettings.setTopicList(options.topics);

            this.questionCount = options.questionCount;
            this.state = loadState(this.theme);
            this.container = $('#question');
            this.indicator = $('#questionLine');
            this.progress = $('#trainingProgress');
            updateProgress(this.progress);

            var questionIndices = [];
            for (var i = 1; i <= this.questionCount; ++i) {
                questionIndices.push(i);
                this.indicator.append('<li data-index="' + i + '"></li>');
            }
            shuffle(questionIndices);
            $.each(questionIndices, function () {
                queue.push(this, true);
            });

            var self = this;
            $('#changeSettings').click($.proxy(TrainingSettings.configure, TrainingSettings));

            this.container.on('click', 'button[name=answer]', function () {
                if (!respondQuestionEvents) return;
                var button = $(this), answer = self.currentQuestion.answers[button.val()];
                var correct = answer.points > 0;

                button.prop('disabled', true);

                if (!self.currentQuestionAnswered) {
                    self.currentQuestionAnswered = true;
                    queue.push(self.currentQuestionIndex, correct);
                    self.indicator.find('li[data-index=' + self.currentQuestionIndex + ']')
                        .toggleClass('correct', correct)
                        .toggleClass('wrong', !correct);

                    progressTotal += 1;
                    progressCorrect += correct ? 1 : 0;
                    updateProgress(self.progress);
                }

                if (correct) {
                    self.showNextQuestion();
                    button.addClass('btn-success');
                } else {
                    button.addClass('btn-danger');
                }
            });
            this.showNextQuestion(0);
        }
    };
})();