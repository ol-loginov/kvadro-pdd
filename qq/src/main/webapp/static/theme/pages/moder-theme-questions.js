var inlineEditOptions = {
    ajaxPost: '/moder/a',
    elementCallbacks: {
        addAnswer: function (el, edit, result, options, defaults) {
            var content = '<li>' +
                '<p data-inline-edit=\'{"edit":"text", "class": "form-control", "params" : {"answer": ' + result.id + ', "name":"answer-text"}}\'>' + result.text + '</p>' +
                '<div class="answer-options">' +
                '<button class="btn btn-xs btn-danger" data-event-handler="deleteAnswer" data-event-param="' + result.id + '">delete</button>' +
                '<button class="btn btn-xs btn-success" data-event-handler="correctAnswer" data-event-param="' + result.id + '">set correct</button></div>' +
                '</li>';
            $(el).closest('.answer-list').find('li:last').before(content);
            inlineEdit.init(inlineEditOptions);
        }
    }
};

inlineEdit.init(inlineEditOptions);

$('ul.answer-list .answer-options')
    .on('click', '[data-event-handler=deleteAnswer]', function () {
        if (confirm("Удалить ответ? Точно?")) {
            var el = $(this), answer = el.data('event-param');
            inlineEdit.post({name: 'delete-answer', answer: answer})
                .done(function () {
                    el.closest('li').remove();
                });
        }
    })
    .on('click', '[data-event-handler=correctAnswer]', function () {
        var el = $(this), answer = el.data('event-param');
        inlineEdit.post({name: 'set-correct-answer', answer: answer})
            .done(function () {
                el.closest('ul').find('li').removeClass('correct-answer');
                el.closest('li').addClass('correct-answer');
            });
    });

$('div.question .question-options')
    .on('click', '[data-event-handler=deleteQuestion]', function () {
        if (confirm("Удалить вопрос? Точно?")) {
            var el = $(this), question = el.data('event-param');
            inlineEdit.post({name: 'delete-question', question: question})
                .done(function () {
                    el.closest('.question').remove();
                });
        }
        return false;
    })
    .on('click', '[data-event-handler=toggleQuestion]', function () {
        var el = $(this), question = el.data('event-param'), current = el.data('event-current');
        inlineEdit.post({name: 'set-question-visibility', question: question, visibility: !current})
            .done(function (resp) {
                el
                    .data('event-current', resp)
                    .toggleClass('btn-success', !resp)
                    .toggleClass('btn-info', resp)
                    .text(resp ? 'выключить' : 'включить')
            });
        return false;
    })
    .on('click', '[data-event-handler=setPicture]', function () {
        var el = $(this), question = el.data('event-param'), questionContainer = el.closest('.question');

        $('<input type="file">')
            .click()
            .change(function () {
                var input = this;
                $(input).remove();

                var formData = new FormData();
                formData.append("name", 'set-picture');
                formData.append("question", question);
                formData.append("file", input.files[0]);

                inlineEdit.post(formData, {processData: false, contentType: false})
                    .done(function (result) {
                        if (result) {
                            questionContainer.find('img.question-image')
                                .attr('src', '/picture/' + result)
                                .toggleClass('has-picture', !!result);
                            questionContainer.find('[data-event-handler=clearPicture]')
                                .prop('disabled', !result);
                        }
                    })
            });
        return false;
    })
    .on('click', '[data-event-handler=clearPicture]', function () {
        var el = $(this), question = el.data('event-param'), questionContainer = el.closest('.question');

        var formData = new FormData();
        formData.append("name", 'set-picture');
        formData.append("question", question);
        formData.append("file", null);

        inlineEdit.post(formData, {processData: false, contentType: false})
            .done(function (result) {
                questionContainer.find('img.question-image')
                    .toggleClass('has-picture', result.length > 0);
                questionContainer.find('[data-event-handler=clearPicture]')
                    .prop('disabled', !(result.length > 0));
            });
        return false;
    });

$('ul.topic-list')
    .on('click', 'button[data-event-handler=toggleTopic]', function () {
        var el = $(this), topic = el.data('event-param'), question = el.data('event-question'), include = el.hasClass('topic-true');
        el.prop('disabled', true);
        inlineEdit.post({name: 'toggle-question-topic', topic: topic, question: question, include: !include})
            .done(function () {
                el.toggleClass('topic-true', !include);
                el.toggleClass('topic-false', include);
                el.prop('disabled', false);
            });
    });
