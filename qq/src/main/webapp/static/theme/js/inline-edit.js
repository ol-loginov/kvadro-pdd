inlineEdit = (function () {
	var defaults = {
		selector: '[data-inline-edit]',
		ajaxHeaders: {},
		elementCallbacks: {}
	};

	function isValueEdit(options) {
		return options.edit == "text";
	}

	function isValueCallback(options) {
		return typeof options.callback != "undefined";
	}

	function callCallback(el, edit, defaults, options, result) {
		var callback = defaults.elementCallbacks[options.callback];
		if ($.isFunction(callback)) {
			callback(el.get(0), edit.get(0), result, options, defaults);
		}
	}

	function performPost(data, options) {
		return $.ajax($.extend({
			url: defaults.ajaxPost,
			method: 'POST',
			data: data,
			headers: defaults.ajaxHeaders
		}, options));
	}

	function submitValue(el, edit, defaults, options) {
		edit.prop('disabled', true);

		var postParams = $.extend(options.params || {}, {value: edit.val()});
		performPost(postParams)
			.done(function (a) {
				if (isValueEdit(options)) {
					el.text(edit.val());
				}
				if (isValueCallback(options)) {
					callCallback(el, edit, defaults, options, a);
				}
			})
			.error(function () {
			})
			.always(function () {
				edit.remove();
				el.show();
			});
	}

	function init(arg) {
		var editOptions = $.extend(defaults, arg);

		$(editOptions.selector).each(function () {
			var el = $(this), options = el.data('inline-edit');
			if (el.data('inline-edit-attached')) {
				return;
			}
			el.data('inline-edit-attached', true);
			el.addClass('edit-inline');
			el.click(function () {
				var edit = $('<textarea></textarea>');
				if (isValueEdit(options)) {
					edit.val(el.text());
				}
				if (options.class) {
					edit.addClass(options.class);
				}

				el
					.hide()
					.after(edit);

				edit.focus();
				edit.blur(function () {
					submitValue(el, edit, editOptions, options);
				});
			})
		});
	}

	return {
		init: init,
		defaults: defaults,
		post: performPost
	}
})();
