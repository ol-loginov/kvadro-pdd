package qq.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

public class AppProperties {
	@Bean
	public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
		PropertySourcesPlaceholderConfigurer bean = new PropertySourcesPlaceholderConfigurer();
		bean.setIgnoreResourceNotFound(true);
		bean.setLocations(
			new ClassPathResource("/qq.properties"),
			new ClassPathResource("/qq-test.properties"));
		return bean;
	}
}
