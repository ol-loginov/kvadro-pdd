package qq.config;

import com.mongodb.MongoURI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import qq.repository.EntityFactory;
import qq.repository.QuestionRepository;
import qq.services.QuestionService;

import java.net.UnknownHostException;

@Configuration
@Import({AppConfigDao.class, AppConfigSecurity.class})
@ComponentScan(basePackageClasses = {EntityFactory.class, QuestionService.class})
public class AppConfig extends AppProperties {
}

@EnableMongoRepositories(basePackageClasses = QuestionRepository.class)
class AppConfigDao {
	@Bean
	public MongoDbFactory mongoDbFactory(@Value("${mongoClientURI}") String mongoClientURI) throws UnknownHostException {
		return new SimpleMongoDbFactory(new MongoURI(mongoClientURI));
	}

	@Bean
	public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory) {
		return new MongoTemplate(mongoDbFactory);
	}
}

@EnableWebSecurity
class AppConfigSecurity extends WebSecurityConfigurerAdapter {
	public void configure(WebSecurity web) {
		web.ignoring()
			.antMatchers("/static/**/*", "/robots.txt", "/favicon.ico", "/BingSiteAuth.xml");
	}

	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("moder").password("jxtymckj;ysqgfhjkm").roles("MODER");
	}

	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/moder", "/moder/**").authenticated();

		http.antMatcher("/moder/**")
			.httpBasic().realmName("Moderator Area");
	}
}