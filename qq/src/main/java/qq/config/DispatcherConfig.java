package qq.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;
import org.springframework.web.servlet.view.velocity.VelocityLayoutViewResolver;
import qq.controllers.IndexController;
import qq.services.FileService;
import qq.support.VelocityLayoutViewWithToolbox;

@Configuration
@ComponentScan(basePackageClasses = IndexController.class)
@Import(DispatcherConfigMvc.class)
public class DispatcherConfig extends AppProperties {
	@Bean
	public Jackson2ObjectMapperBuilder jacksonBuilder() {
		return new Jackson2ObjectMapperBuilder()
			.indentOutput(true)
			.featuresToEnable(JsonGenerator.Feature.QUOTE_FIELD_NAMES)
			.featuresToEnable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES)
			.featuresToEnable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
			.featuresToEnable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
			.featuresToDisable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
			.serializationInclusion(JsonInclude.Include.NON_NULL);
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver bean = new CommonsMultipartResolver();
		bean.setMaxUploadSize(1024 * 1000);
		return bean;
	}

	@Bean
	public MultipartFilter multipartFilter() {
		return new MultipartFilter();
	}
}

@EnableWebMvc
class DispatcherConfigMvc extends WebMvcConfigurerAdapter {
	@Autowired
	private FileService fileService;

	@Bean
	public VelocityLayoutViewResolver velocityLayoutViewResolver() {
		VelocityLayoutViewResolver viewResolver = new VelocityLayoutViewResolver();
		viewResolver.setViewClass(VelocityLayoutViewWithToolbox.class);
		viewResolver.setSuffix(".vm");
		viewResolver.setContentType("text/html; charset=UTF-8");
		viewResolver.setExposeRequestAttributes(true);
		viewResolver.setDateToolAttribute("dateTool");
		viewResolver.setLayoutUrl("layout/default.vm");
		viewResolver.setToolboxConfigLocation("velocity-tools.xml");
		viewResolver.setCache(false);
		viewResolver.setCacheUnresolved(false);
		return viewResolver;
	}

	@Bean
	public VelocityConfigurer velocityConfigurer() {
		VelocityConfigurer configurer = new VelocityConfigurer();
		configurer.setResourceLoaderPath("/WEB-INF/velocity/");
		configurer.setConfigLocation(new ClassPathResource("/velocity.properties"));
		return configurer;
	}

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.setOrder(0);
		registry.addResourceHandler("/static/**").addResourceLocations("/static/");
		registry.addResourceHandler("/picture/**").addResourceLocations("file://" + fileService.getRootFolder() + "/");
		registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
		registry.addResourceHandler("/favicon.ico").addResourceLocations("/favicon.ico");
		registry.addResourceHandler("/robots.txt").addResourceLocations("/robots.txt");
	}
}