package qq.controllers.view;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;

@Getter
@Setter
public class EditThemeForm {
	private boolean createMode = true;
	@NotBlank
	@Pattern(regexp = "[-_a-zA-Z0-9]+")
	private String url;
	@NotBlank
	private String title;
}
