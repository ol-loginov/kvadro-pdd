package qq.controllers.view;

import lombok.Getter;
import qq.repository.Answer;
import qq.repository.Question;

import java.util.List;

@Getter
public class QuestionView {
	private final Question question;
	private final List<Answer> answers;

	public QuestionView(Question question, List<Answer> answers) {
		this.question = question;
		this.answers = answers;
	}

	public boolean hiddenFromUser() {
		return !question.isVisible();
	}
}
