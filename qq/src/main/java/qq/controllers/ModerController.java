package qq.controllers;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import qq.controllers.view.EditThemeForm;
import qq.controllers.view.QuestionView;
import qq.repository.*;
import qq.services.QuestionService;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Controller
@RequestMapping("/moder")
public class ModerController extends ControllerResults {
	@Autowired
	ThemeRepository themeRepository;
	@Autowired
	QuestionRepository questionRepository;
	@Autowired
	AnswerRepository answerRepository;
	@Autowired
	QuestionService questionService;
	@Autowired
	TicketRepository ticketRepository;
	@Autowired
	TopicRepository topicRepository;
	@Autowired
	EntityFactory entityFactory;

	@RequestMapping
	public ModelAndView moder() {
		return view("moder")
			.addObject("themes", themeRepository.findAll(Theme.BY_CREATED_DESC));
	}

	@RequestMapping(value = "/themes/add", method = RequestMethod.GET)
	public ModelAndView addThemePage() {
		return view("moder-theme-add")
			.addObject("form", new EditThemeForm());
	}

	@RequestMapping(value = "/themes/add", method = RequestMethod.POST)
	public ModelAndView addTheme(@Valid EditThemeForm form, BindingResult bindingErrors) {
		if (bindingErrors.hasErrors()) {
			return view("moder-theme-add")
				.addObject("form", form)
				.addObject("formErrors", bindingErrors);
		} else {
			Theme theme = entityFactory.newTheme(form.getUrl());
			theme.setTitle(form.getTitle());
			themeRepository.save(theme);
			return redirect("/moder");
		}
	}

	private Iterable<QuestionView> prepareQuestionView(Iterable<Question> questionList) {
		List<QuestionView> result = new ArrayList<>();
		for (Question q : questionList) {
			result.add(new QuestionView(q, answerRepository.findAllByQuestion(q.getId())));
		}
		return result;
	}

	Map<Integer, String> prepareQuestionTitles(Iterable<Question> questionList) {
		Map<Integer, String> map = new TreeMap<>();
		for (Question q : questionList) {
			map.put(q.getId(), q.getText());
		}
		return map;
	}

	@RequestMapping(value = "/theme/{themeId}/questions", method = RequestMethod.GET)
	public ModelAndView editThemeQuestions(@PathVariable String themeId) {
		return view("moder-theme-questions")
			.addObject("theme", themeRepository.findOne(themeId))
			.addObject("questionList", prepareQuestionView(questionRepository.findAllByTheme(themeId, Question.BY_CREATED_DESC)))
			.addObject("topicList", topicRepository.findAllByTheme(themeId, Topic.BY_TITLE));
	}

	@RequestMapping(value = "/theme/{themeId}/questions", method = RequestMethod.POST)
	public ModelAndView addThemeQuestion(@PathVariable String themeId, @Valid @NotBlank String text) {
		questionService.addQuestion(themeRepository.findOne(themeId), text);
		return redirect("/moder/theme/" + themeId + "/questions");
	}

	@RequestMapping(value = "/theme/{themeId}/tickets", method = RequestMethod.GET)
	public ModelAndView viewThemeTickets(@PathVariable String themeId) {
		return view("moder-theme-tickets")
			.addObject("theme", themeRepository.findOne(themeId))
			.addObject("questionList", prepareQuestionTitles(questionRepository.findAllByTheme(themeId, Question.BY_CREATED_ASC)))
			.addObject("ticketList", ticketRepository.findAllByTheme(themeId));
	}

	@RequestMapping(value = "/theme/{themeId}/tickets", method = RequestMethod.POST)
	public ModelAndView addThemeTicket(@PathVariable String themeId, @Valid @NotBlank String text) {
		questionService.addTicket(themeRepository.findOne(themeId), text);
		return redirect("/moder/theme/" + themeId + "/tickets");
	}

	@RequestMapping(value = "/ticket/{ticketId}/questions", method = RequestMethod.GET)
	public ModelAndView selectTicketQuestion(@PathVariable int ticketId) {
		Ticket ticket = ticketRepository.findOne(ticketId);
		return view("moder-theme-ticket-questions")
			.addObject("theme", themeRepository.findOne(ticket.getTheme()))
			.addObject("ticket", ticket)
			.addObject("questionList", prepareQuestionTitles(questionRepository.findAllByTheme(ticket.getTheme(), Question.BY_CREATED_ASC)));
	}

	@RequestMapping(value = "/ticket/{ticketId}/questions", method = RequestMethod.POST)
	public ModelAndView changeTicketQuestion(@PathVariable int ticketId, @RequestParam(required = false) int[] questions) {
		Ticket ticket = ticketRepository.findOne(ticketId);
		questionService.setTicketQuestions(ticket, questions);
		return redirect("/moder/theme/" + ticket.getTheme() + "/tickets");
	}

	@RequestMapping(value = "/theme/{themeId}/topics", method = RequestMethod.GET)
	public ModelAndView editThemeTopics(@PathVariable String themeId) {
		return view("moder-theme-topics")
			.addObject("theme", themeRepository.findOne(themeId))
			.addObject("topicList", topicRepository.findAllByTheme(themeId, Topic.BY_TITLE));
	}

	@RequestMapping(value = "/theme/{themeId}/topics", method = RequestMethod.POST)
	public ModelAndView addThemeTopic(@PathVariable String themeId, @Valid @NotBlank String text) {
		questionService.addTopic(themeRepository.findOne(themeId), text);
		return redirect("/moder/theme/" + themeId + "/topics");
	}
}

@RestController
@RequestMapping(value = "/moder/a", method = RequestMethod.POST)
class ModerControllerRest {
	@Autowired
	ThemeRepository themeRepository;
	@Autowired
	QuestionRepository questionRepository;
	@Autowired
	AnswerRepository answerRepository;
	@Autowired
	QuestionService questionService;
	@Autowired
	TicketRepository ticketRepository;
	@Autowired
	TopicRepository topicRepository;
	@Autowired
	EntityFactory entityFactory;

	@RequestMapping(params = "name=question-text")
	public boolean changeQuestionText(@RequestParam int question, @RequestParam String value) {
		questionRepository.updateText(questionRepository.findOne(question), value);
		return true;
	}

	@RequestMapping(params = "name=answer-text")
	public boolean changeAnswerText(@RequestParam int answer, @RequestParam String value) {
		answerRepository.updateText(answerRepository.findOne(answer), value);
		return true;
	}

	@RequestMapping(params = "name=new-answer")
	public Answer createAnswer(@RequestParam int question, @RequestParam String value) {
		return questionService.addAnswer(questionRepository.findOne(question), value);
	}

	@RequestMapping(params = "name=delete-answer")
	public boolean deleteAnswer(@RequestParam int answer) {
		questionService.deleteAnswer(answerRepository.findOne(answer));
		return true;
	}

	@RequestMapping(params = "name=delete-question")
	public boolean deleteQuestion(@RequestParam int question) {
		questionService.deleteQuestion(questionRepository.findOne(question));
		return true;
	}

	@RequestMapping(params = "name=delete-ticket")
	public boolean deleteTicket(@RequestParam int ticket) {
		questionService.deleteTicket(ticketRepository.findOne(ticket));
		return true;
	}

	@RequestMapping(params = "name=delete-topic")
	public boolean deleteTopic(@RequestParam int topic) {
		questionService.deleteTopic(topicRepository.findOne(topic));
		return true;
	}

	@RequestMapping(params = "name=set-picture")
	public String setPicture(@RequestParam int question, @RequestParam(required = false) MultipartFile file) throws IOException {
		InputStream fileStream = file == null ? null : file.getInputStream();
		return questionService.setQuestionPicture(questionRepository.findOne(question), fileStream);
	}

	@RequestMapping(params = "name=set-correct-answer")
	public boolean setCorrectAnswer(@RequestParam int answer) {
		questionService.setAnswerPoints(answerRepository.findOne(answer), 1, true);
		return true;
	}

	@RequestMapping(params = "name=set-question-visibility")
	public boolean setCorrectAnswer(@RequestParam int question, @RequestParam boolean visibility) {
		return questionService
			.setQuestionVisible(questionRepository.findOne(question), visibility)
			.isVisible();
	}

	@RequestMapping(params = "name=toggle-question-topic")
	public boolean toggleQuestionTopic(@RequestParam int question, @RequestParam int topic, @RequestParam boolean include) {
		questionService.setQuestionTopic(questionRepository.findOne(question), topicRepository.findOne(topic), include);
		return true;
	}

	@RequestMapping(params = "name=toggle-question-shuffle")
	public boolean toggleQuestionShuffle(@RequestParam int question, @RequestParam boolean enable) {
		questionService.setQuestionShuffle(questionRepository.findOne(question), enable);
		return true;
	}
}