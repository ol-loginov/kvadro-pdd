package qq.controllers;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

public abstract class ControllerResults {
	public ModelAndView view(String viewName) {
		return new ModelAndView(viewName);
	}

	public ModelAndView redirect(String url) {
		return view(UrlBasedViewResolver.REDIRECT_URL_PREFIX + url);
	}
}
