package qq.controllers;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import qq.repository.Theme;
import qq.repository.ThemeRepository;
import qq.repository.Topic;
import qq.repository.TopicRepository;
import qq.services.TrainingQuestion;
import qq.services.TrainingService;

@Controller
public class IndexController extends ControllerResults {
	@Autowired
	ThemeRepository themeRepository;
	@Autowired
	TopicRepository topicRepository;
	@Autowired
	TrainingService trainingService;

	@RequestMapping({"/", "/index.html"})
	public ModelAndView index() {
		return view("index")
			.addObject("themes", themeRepository.findAll());
	}

	@RequestMapping(value = "/{themeId}/training", method = RequestMethod.GET)
	public ModelAndView takeTraining(@PathVariable String themeId) {
		Theme theme = themeRepository.findOne(themeId);
		return view("theme-training")
			.addObject("theme", theme)
			.addObject("topics", trainingService.getThemeTopicCounters(themeId))
			.addObject("questionCount", trainingService.getThemeQuestionCount(theme));
	}
}

@RestController
@RequestMapping("/a")
class IndexControllerApi {
	@Autowired
	TrainingService trainingService;

	@RequestMapping(params = "name=get-question", method = RequestMethod.POST)
	public TrainingQuestion submitTrainingQuestion(@RequestParam String theme, @RequestParam int question) {
		return trainingService.prepareQuestion(theme, question);
	}
}