package qq.support;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.lang.StringEscapeUtils;

public class HtmlTool {
	private final ObjectMapper objectMapper = new ObjectMapper();

	public HtmlTool() {
		objectMapper
			.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true)
			.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
			.configure(SerializationFeature.FAIL_ON_SELF_REFERENCES, true)
			.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false)
			.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
	}

	public String toJsList(Iterable<?> source) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		if (source != null) {
			for (Object s : source) {
				if (s != null) {
					if (builder.length() > 1) {
						builder.append(",");
					}
					builder
						.append("'")
						.append(StringEscapeUtils.escapeJavaScript(s.toString()))
						.append("'");
				}
			}
		}
		return builder.append("]").toString();
	}

	public String toJsObjectList(Iterable<?> source) throws JsonProcessingException {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		if (source != null) {
			for (Object s : source) {
				if (s != null) {
					if (builder.length() > 1) {
						builder.append(",");
					}
					builder.append(toJsObject(s));
				}
			}
		}
		return builder.append("]").toString();
	}

	private String toJsObject(Object o) throws JsonProcessingException {
		if (o == null) {
			return "null";
		}
		return objectMapper.writeValueAsString(o);
	}
}
