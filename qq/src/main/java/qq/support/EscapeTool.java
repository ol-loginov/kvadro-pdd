package qq.support;

import org.apache.commons.lang.StringEscapeUtils;

public class EscapeTool {
	public String html(String input) {
		if (input == null) {
			return null;
		}
		return StringEscapeUtils.escapeHtml(input);
	}
}
