package qq.support;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTool {
	public String format(Date date, String formatString, Locale locale) {
		return new SimpleDateFormat(formatString, locale).format(date);
	}
}
