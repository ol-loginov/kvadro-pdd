package qq.services;

import lombok.AllArgsConstructor;
import lombok.Getter;
import qq.repository.Answer;
import qq.repository.Question;

import java.util.List;

@Getter
@AllArgsConstructor
public class TrainingQuestion {
	Question question;
	List<Answer> answers;
	List<String> topics;
}
