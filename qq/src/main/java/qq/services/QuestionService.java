package qq.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import qq.repository.*;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface QuestionService {
	Question setQuestionVisible(Question question, boolean visible);

	Ticket setTicketQuestions(Ticket ticket, int[] questions);

	Ticket addTicket(Theme theme, String title);

	void deleteTicket(Ticket ticket);

	void setAnswerPoints(Answer answer, int points, boolean clearOthers);

	String setQuestionPicture(Question question, InputStream stream) throws IOException;

	void deleteQuestion(Question question);

	void deleteAnswer(Answer answer);

	Question addQuestion(Theme theme, String questionText);

	Answer addAnswer(Question question, String answerText);

	Topic addTopic(Theme theme, String title);

	void deleteTopic(Topic topic);

	void setQuestionTopic(Question question, Topic topic, boolean include);

	void setQuestionShuffle(Question question, boolean enable);
}


@Service
class QuestionServiceImpl implements QuestionService {
	@Autowired
	EntityFactory entityFactory;
	@Autowired
	FileService fileService;
	@Autowired
	QuestionRepository questionRepository;
	@Autowired
	ThemeRepository themeRepository;
	@Autowired
	TicketRepository ticketRepository;
	@Autowired
	TopicRepository topicRepository;
	@Autowired
	AnswerRepository answerRepository;

	public Question addQuestion(Theme theme, String textData) {
		List<String> textLines = new ArrayList<>();
		Collections.addAll(textLines, textData.split("\n"));

		Question question = entityFactory.newQuestion(theme);
		question.setText(textLines.remove(0));
		questionRepository.save(question);
		themeRepository.changeQuestionCount(theme, 1);

		for (String line : textLines) {
			if (!line.isEmpty()) {
				addAnswer(question, line);
			}
		}

		return question;
	}

	public void deleteQuestion(Question question) {
		questionRepository.delete(question);
		answerRepository.deleteByQuestion(question.getId());
		themeRepository.changeQuestionCount(themeRepository.findOne(question.getTheme()), -1);
		ticketRepository.deleteTicketQuestion(question);
	}

	@Override
	public Topic addTopic(Theme theme, String title) {
		Topic topic = entityFactory.newTopic(theme, title);
		topicRepository.save(topic);
		return topic;
	}

	public Ticket addTicket(Theme theme, String title) {
		Ticket ticket = entityFactory.newTicket(theme, title);
		ticketRepository.save(ticket);
		themeRepository.changeTicketCount(theme, 1);
		return ticket;
	}

	public void deleteTicket(Ticket ticket) {
		ticketRepository.delete(ticket);
		themeRepository.changeQuestionCount(themeRepository.findOne(ticket.getTheme()), -1);
	}

	public Answer addAnswer(Question question, String answerText) {
		String stripped = StringUtils.trimWhitespace(answerText);
		if (stripped.isEmpty()) {
			return null;
		}

		Answer answer = entityFactory.newAnswer(question);
		answer.setText(stripped);
		answerRepository.save(answer);
		return answer;
	}

	public void deleteAnswer(Answer answer) {
		answerRepository.delete(answer);
	}

	public String setQuestionPicture(Question question, InputStream stream) throws IOException {
		if (stream == null) {
			String oldPicture = questionRepository.updatePicture(question, null);
			fileService.deleteFile(oldPicture);
			return null;
		} else {
			String picture = fileService.createFile(ImageIO.read(stream));
			String oldPicture = questionRepository.updatePicture(question, picture);
			fileService.deleteFile(oldPicture);
			return picture;
		}
	}


	public Question setQuestionVisible(Question question, boolean visible) {
		questionRepository.updateVisibility(question, visible);
		return question;
	}

	public void setAnswerPoints(Answer answer, int points, boolean clearOthers) {
		if (clearOthers) {
			Question question = questionRepository.findOne(answer.getQuestion());
			answerRepository.updatePoints(question, 0);
		}
		answerRepository.updatePoints(answer, points);
	}

	public Ticket setTicketQuestions(Ticket ticket, int[] questions) {
		ticketRepository.updateQuestions(ticket, questions);
		return ticket;
	}

	@Override
	public void deleteTopic(Topic topic) {
		topicRepository.delete(topic);
		questionRepository.removeTopicFromQuestions(topic);
	}

	@Override
	public void setQuestionTopic(Question question, Topic topic, boolean include) {
		questionRepository.updateQuestionTopic(question,topic,include);
	}

	@Override
	public void setQuestionShuffle(Question question, boolean enable) {
		questionRepository.updateShuffle(question,enable);
	}
}
