package qq.services;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import qq.repository.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class TrainingService {
	@Autowired
	QuestionRepository questionRepository;
	@Autowired
	AnswerRepository answerRepository;
	@Autowired
	TopicRepository topicRepository;

	public int getThemeQuestionCount(Theme theme) {
		return questionRepository.countPublicQuestions(theme.getId());
	}

	public TrainingQuestion prepareQuestion(String theme, int questionNumber) {
		Question question = questionRepository.findPublicQuestion(theme, new PageRequest(questionNumber - 1, 1, Sort.Direction.ASC, "createdAt")).iterator().next();
		List<Answer> answers = answerRepository.findAllByQuestion(question.getId());
		List<String> topics = new ArrayList<>();
		for (int t : question.getTopics()) {
			Topic topic = topicRepository.findOne(t);
			if (topic != null) {
				topics.add(topic.getTitle());
			}
		}
		return new TrainingQuestion(question, answers, topics);
	}

	public Collection<TrainingTopic> getThemeTopicCounters(final String theme) {
		Collection<TrainingTopic> result = CollectionUtils.collect(topicRepository.findAllByTheme(theme, Topic.BY_TITLE), new Transformer<Topic, TrainingTopic>() {
			@Override
			public TrainingTopic transform(Topic input) {
				return new TrainingTopic(input.getTitle(), questionRepository.countTopicQuestions(theme, input.getId()));
			}
		});
		result.add(new TrainingTopic("", questionRepository.countNoTopicQuestions(theme)));
		return result;
	}
}
