package qq.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public interface FileService {
	void deleteFile(String filePath) throws IOException;

	String createFile(BufferedImage image) throws IOException;

	Path getRootFolder();
}

@Service
class FileServiceImpl implements FileService {
	@Value("${FileService.fileFolder}")
	private String folder;
	private Path rootFolder;

	@PostConstruct
	public void createFolders() throws IOException {
		rootFolder = Paths.get(folder);
		if (!Files.exists(rootFolder)) {
			Files.createDirectories(rootFolder);
		}
	}

	@Override
	public Path getRootFolder() {
		return rootFolder;
	}

	public void deleteFile(String filePath) throws IOException {
		if (filePath != null) {
			Files.deleteIfExists(Paths.get(folder, filePath));
		}
	}

	public String createFile(BufferedImage image) throws IOException {
		if (image == null) {
			return null;
		}

		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS", Locale.ENGLISH);
		String fileName = df.format(new Date()) + ".png";
		String fileFolder = fileName.substring(0, 6);

		Path fileRelativeFile = Paths.get(fileFolder, fileName);
		Path fileAbsoluteFile = rootFolder.resolve(fileRelativeFile);
		Files.createDirectories(fileAbsoluteFile.getParent());
		ImageIO.write(image, "png", fileAbsoluteFile.toFile());

		return fileRelativeFile.toString();
	}
}
