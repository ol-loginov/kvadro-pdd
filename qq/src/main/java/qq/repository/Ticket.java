package qq.repository;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "Ticket")
@TypeAlias("Ticket")
@Getter
@Setter
public class Ticket {
	@Id
	int id;
	String theme;
	String title;
	@CreatedDate
	Date createdAt = new Date();
	int[] questions = new int[0];

	public Ticket(int id, String theme, String title) {
		this.id = id;
		this.theme = theme;
		this.title = title;
	}
}
