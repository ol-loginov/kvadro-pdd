package qq.repository;

public interface ThemeRepositoryExt {
	void changeQuestionCount(Theme theme, int diff);

	void changeTicketCount(Theme theme, int diff);
}
