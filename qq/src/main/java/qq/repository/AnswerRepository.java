package qq.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends MongoRepository<Answer, Integer>,AnswerRepositoryExt {
	int deleteByQuestion(int question);

	List<Answer> findAllByQuestion(int question);
}
