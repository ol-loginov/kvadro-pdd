package qq.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public interface ThemeRepository extends MongoRepository<Theme, String>, ThemeRepositoryExt {
}

@Service
class ThemeRepositoryImpl implements ThemeRepositoryExt {
	@Autowired
	MongoTemplate mongoTemplate;

	public void changeQuestionCount(Theme theme, int diff) {
		mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(theme.getId())), new Update().inc("questions", diff), Theme.class);
		theme.setQuestions(theme.getQuestions() + diff);
	}

	public void changeTicketCount(Theme theme, int diff) {
		mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(theme.getId())), new Update().inc("tickets", diff), Theme.class);
		theme.setTickets(theme.getTickets() + diff);
	}
}

