package qq.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends MongoRepository<Question, Integer>, QuestionRepositoryExt {
	@Query(value = "{visible: true, theme: ?0}", count = true)
	int countPublicQuestions(String theme);

	@Query("{visible: true, theme: ?0}")
	Page<Question> findPublicQuestion(String theme, Pageable pageable);

	List<Question> findAllByTheme(String theme, Sort sort);

	@Query(value = "{visible: true, theme: ?0, topics: ?1}", count = true)
	long countTopicQuestions(String theme, int topic);
}
