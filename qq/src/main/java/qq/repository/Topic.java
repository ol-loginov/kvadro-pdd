package qq.repository;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.Transformer;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Topic")
@TypeAlias("Topic")
@Getter
@Setter
public class Topic {
	public static final Sort BY_TITLE = new Sort(Sort.Direction.ASC, "title");
	public static final Transformer<Topic, String> GET_TITLE = new Transformer<Topic, String>() {
		@Override
		public String transform(Topic input) {
			return input.getTitle();
		}
	};

	@Id
	private int id;
	private String theme;
	private String title;

	public Topic(int id, String theme, String title) {
		this.id = id;
		this.theme = theme;
		this.title = title;
	}
}
