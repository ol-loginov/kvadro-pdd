package qq.repository;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.CollectionCallback;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class EntityFactory {
	public static final String COLLECTION_NAME = "Seq";
	public static final String NUMBER_FIELD = "seq";

	@Autowired
	private MongoTemplate mongoTemplate;

	private static String sequenceNameOf(Class<?> sequenceTarget) {
		return sequenceTarget.getSimpleName();
	}

	private int sequenceIncrement(Class<?> sequenceTarget) {
		final Query sequence = Query.query(Criteria.where("_id").is(sequenceNameOf(sequenceTarget)));
		final Update update = new Update().inc(NUMBER_FIELD, 1);
		return mongoTemplate.execute(COLLECTION_NAME, new CollectionCallback<Integer>() {
			public Integer doInCollection(DBCollection collection) {
				DBObject result = collection.findAndModify(sequence.getQueryObject(), null, null, false, update.getUpdateObject(), true, false);
				return ((Number) result.get(NUMBER_FIELD)).intValue();
			}
		});
	}

	private void ensureSequenceInit(Class<?> sequenceTarget) {
		if (!mongoTemplate.exists(Query.query(Criteria.where("_id").is(sequenceNameOf(sequenceTarget))), COLLECTION_NAME)) {
			mongoTemplate.upsert(Query.query(Criteria.where("_id").is(sequenceNameOf(sequenceTarget))), Update.update("seq", 0), COLLECTION_NAME);
		}
	}

	@PostConstruct
	public void createNeededSequences() {
		ensureSequenceInit(Question.class);
		ensureSequenceInit(Answer.class);
		ensureSequenceInit(Ticket.class);
		ensureSequenceInit(Topic.class);
	}

	public Question newQuestion(Theme theme) {
		return new Question(sequenceIncrement(Question.class), theme.getId());
	}

	public Ticket newTicket(Theme theme, String title) {
		return new Ticket(sequenceIncrement(Ticket.class), theme.getId(), title);
	}

	public Answer newAnswer(Question question) {
		return new Answer(sequenceIncrement(Answer.class), question.getId());
	}

	public Theme newTheme(String url) {
		return new Theme(url);
	}

	public Topic newTopic(Theme theme, String title) {
		return new Topic(sequenceIncrement(Topic.class), theme.getId(), title);
	}
}
