package qq.repository;

import com.mongodb.WriteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

public interface TicketRepositoryExt {
	void updateQuestions(Ticket instance, int[] questions);

	int deleteTicketQuestion(Question question);
}

@Service
class TicketRepositoryImpl implements TicketRepositoryExt {
	@Autowired
	MongoTemplate mongoTemplate;

	public void updateQuestions(Ticket instance, int[] questions) {
		WriteResult res = mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(instance.id)), Update.update("questions", questions), Ticket.class);
		if (res.getN() == 1) {
			instance.questions = questions;
		}
	}

	public int deleteTicketQuestion(Question question) {
		return mongoTemplate
			.updateFirst(new Query(), new Update().pull("questions", question.getId()), Ticket.class)
			.getN();
	}
}