package qq.repository;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "Answer")
@TypeAlias("Answer")
public class Answer {
	@Id
	private int id;
	private int question;
	private int points;
	private String picture;
	private String text;

	public Answer(int id, int question) {
		this.id = id;
		this.question = question;
	}

	public boolean isCorrect() {
		return points > 0;
	}
}
