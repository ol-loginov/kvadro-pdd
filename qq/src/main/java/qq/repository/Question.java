package qq.repository;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "Question")
@TypeAlias("Question")
@Getter
@Setter
public class Question {
	public static final Sort BY_CREATED_DESC = new Sort(Sort.Direction.DESC, "createdAt");
	public static final Sort BY_CREATED_ASC = new Sort(Sort.Direction.ASC, "createdAt");

	@Id
	private int id;
	private String theme;
	@CreatedDate
	private Date createdAt = new Date();
	private boolean visible = false;
	private String picture;
	private String text;
	private boolean shuffle = true;
	private int[] topics = new int[0];

	public Question(int id, String theme) {
		this.id = id;
		this.theme = theme;
	}

	public boolean hasPicture() {
		return picture != null;
	}

	public boolean hasTopic(int topic) {
		for (int t : topics) {
			if (t == topic) {
				return true;
			}
		}
		return false;
	}
}
