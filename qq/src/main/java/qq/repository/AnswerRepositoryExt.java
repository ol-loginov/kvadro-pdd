package qq.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

public interface AnswerRepositoryExt {
	public void updateText(Answer instance, String text);

	public void updatePoints(Answer answer, int points);

	public int updatePoints(Question question, int points);
}

@Service
class AnswerRepositoryImpl implements AnswerRepositoryExt {
	@Autowired
	MongoTemplate mongoTemplate;

	public void updateText(Answer instance, String text) {
		mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(instance.getId())), Update.update("text", text), Answer.class);
	}

	public void updatePoints(Answer answer, int points) {
		mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(answer.getId())), Update.update("points", points), Answer.class);
		answer.setPoints(points);
	}

	public int updatePoints(Question question, int points) {
		return mongoTemplate
			.updateMulti(Query.query(Criteria.where("question").is(question.getId())), Update.update("points", points), Answer.class)
			.getN();
	}
}
