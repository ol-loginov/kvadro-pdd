package qq.repository;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "Theme")
@TypeAlias("Theme")
@Getter
@Setter
public class Theme {
	public static final Sort BY_CREATED_DESC = new Sort(Sort.Direction.DESC, "createdAt");

	@Id
	private String id;
	private String title;
	@CreatedDate
	private Date createdAt = new Date();
	private int tickets = 0;
	private int questions = 0;

	public Theme(String id) {
		this.id = id;
	}
}
