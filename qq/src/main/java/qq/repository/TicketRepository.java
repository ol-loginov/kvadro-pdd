package qq.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends MongoRepository<Ticket, Integer>, TicketRepositoryExt {
	List<Ticket> findAllByTheme(String theme);
}
