package qq.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

public interface QuestionRepositoryExt {
	public void updateText(Question question, String text);

	public String updatePicture(Question question, String value);

	public void updateVisibility(Question question, boolean visibility);

	int removeTopicFromQuestions(Topic topic);

	boolean updateQuestionTopic(Question question, Topic topic, boolean include);

	void updateShuffle(Question question, boolean enable);

	long countNoTopicQuestions(String theme);
}

@Service
class QuestionRepositoryImpl implements QuestionRepositoryExt {
	@Autowired
	MongoTemplate mongoTemplate;

	public void updateText(Question question, String text) {
		mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(question.getId())), Update.update("text", text), Question.class);
		question.setText(text);
	}

	public void updateVisibility(Question question, boolean visibility) {
		mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(question.getId())), Update.update("visible", visibility), Question.class);
		question.setVisible(visibility);
	}

	public String updatePicture(Question question, String value) {
		String oldPicture = question.getPicture();
		mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(question.getId())), Update.update("picture", value), Question.class);
		question.setPicture(value);
		return oldPicture;
	}

	public int removeTopicFromQuestions(Topic topic) {
		return mongoTemplate
			.updateFirst(new Query(), new Update().pull("topics", topic.getId()), Question.class)
			.getN();
	}

	@Override
	public boolean updateQuestionTopic(Question question, Topic topic, boolean include) {
		Update update;
		if (include) {
			update = new Update().addToSet("topics", topic.getId());
		} else {
			update = new Update().pull("topics", topic.getId());
		}
		return mongoTemplate
			.updateFirst(Query.query(Criteria.where("_id").is(question.getId())), update, Question.class)
			.getN() == 1;
	}

	@Override
	public void updateShuffle(Question question, boolean enable) {
		mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(question.getId())), Update.update("shuffle", enable), Question.class);
		question.setShuffle(enable);
	}

	@Override
	public long countNoTopicQuestions(String theme) {
		Criteria criteria = Criteria
			.where("visible").is(true)
			.and("theme").is(theme)
			.orOperator(
				Criteria.where("topics").exists(false),
				Criteria.where("topics").size(0));
		return mongoTemplate.count(Query.query(criteria), Question.class);
	}
}
